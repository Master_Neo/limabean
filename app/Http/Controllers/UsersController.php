<?php

namespace App\Http\Controllers;

//use Request;
use \App\Models\Users;
use Illuminate\Http\Request;

class UsersController extends Controller
{
	
	public function index(){	
		
		$this_array = array(14,23,264,654,4,34,34);
		//prepare return type
		$sum = 0;
		//loop and accumulate numerals
		foreach($this_array as $key){
			$sum = $sum + $key;			
		}
		
		$person_array = array('Leanna', 'derek', 'Lisa', 'John', 'lancelot', 'Michael', 'norman', 'Lawrence of Arabia');
		//search key L or l
		$searchkey = '';
		//found words/names array list 
		$new_array = [];
		
		//iterate person array , substring fron index 0 to 1, 
		//search and extract for a matching character starting with L or l 
		//push found key to a new array and 
		//return json array to consumer end point ...
		foreach($person_array as $key){
			$searchkey = substr($key,0,1);				
			if($searchkey === 'L' || $searchkey === 'l' )
			{
				array_push($new_array,$key);
			}		
		}		
		
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		
		$id = "8,9,1,0,2,9,5,2,0,9,8,7,1";
		$arr = explode(',',$id);
		
		
		$idnum = array_unique($arr);
		
		// reverse sort elements 
		$sorted = rsort($arr, SORT_NUMERIC);
		
		//get riddcollect and exclude duplicates
		$dups  = [];
		$idnum = array_unique($arr);
		$i=0;
		$zeros=0;
		//find id and add zeros 
		foreach($idnum as $id)
		{
			$i++;
			$dup = "dup". $i;			 
			 array_push($dups,$id);
			 $zeros = 13 - $i;
		}
		//members
		$z=0;
		$z = $zeros;		
		$zero = '0';
		$dup = " dup ";
		$k=1;
		//push zero and fill gap to be 13 in length
		for($j = 0 ; $j < $zeros ; $j++)
		{		
			$k++;
			$dup = "dup".($z + $k);			 
			array_push($dups,$zero);
		}
		//return a string ID Number instead ...
		$str = '';
		foreach($dups as $id)
		{
			 $str = $str.$id;
		}	
		//converse of the inverse,  array to string ...
		$iddup = implode(',',$dups);
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//psginate two persons per page 
		// get user Entity 
		//return json with all required data to End poin Interface ...
		$users = \App\Models\Users::paginate(2);
		
		return response()->json([
		"msg"=>"Successfully retrieved users!",//flag
		"newarray"=>$new_array,//answer for question 3
		"strid"=>$str, //question4
		"dups"=>$dups, //
		"sum"=>$sum,//sum of all elements in array 
		"users" =>$users->toArray()	//List person 	
		],200 // okay status code, meaning request was fullfilled   
		);
	}

	public function store(Request $request){
		
		$users =  new \App\Models\Users();		
		//members to persit / entity props
		$users->firstname =  $request->firstname;
		$users->idnumber =  $request->idnumber;
		$users->surname = $request->surname;
		
		$users->save();//orm / nosql / 
		
		return response()->json([
		"msg"=>"Successfully saved person!"
		],200
		);
	}
	}
	?>
	
